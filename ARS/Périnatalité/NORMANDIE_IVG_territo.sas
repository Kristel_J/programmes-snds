option mprint symbolgen;
/********************************************************************/
/****Estimation  du nombre d'IVG en ville + hopital 		*/
/* profil 84 patient 						*/                 
/*****************************************************************/


/*1�re partie : DCIR*/
 /* les IVG en ville */ 
/* pour info, on ne met pas de filtre sur le top T2A car il s'agit de forfait ville exclusivement */

proc sql; 
drop table sasdata1.ivg; 
%connectora;
create table sasdata1.ivg as 
select * from connection to oracle (
select 
ben_nir_psa,ben_rng_gem, ben_ama_cod,ben_res_dpt,ben_res_com, sum(case when PRS_nat_ref='1981' then PRS_ACT_QTE else 0 end) as qtt_forf_med,
sum(case when PRS_nat_ref='3329' then PRS_ACT_QTE else 0 end) as qtt_forf_pharm
from er_prs_f  
where prs_nat_ref in ('1981', '3329')
and flx_dis_dtd>to_date('01012017','DDMMYYYY')
 AND dpn_qlf not in (71)
AND CPL_MAJ_TOP in (0,1)
       AND EXE_SOI_DTD BETWEEN to_date('01012017','DDMMYYYY') AND to_date('31122017','DDMMYYYY') 
and ben_res_dpt in ('014','050','061','027','076')
GROUP bY ben_nir_psa,ben_rng_gem, ben_ama_cod,ben_res_dpt,ben_res_com);
disconnect from oracle; 
quit; 



proc sql;
create table sasdata1.ivg2 as select 
trim((substr(ben_res_dpt,2,2)||ben_res_com))as codcom,
case 
		when (BEN_AMA_COD < 18 AND BEN_AMA_COD> 150) then "[0-17]"
		when (BEN_AMA_COD >= 18 AND BEN_AMA_COD<=24) then "[18-24]"
		when (BEN_AMA_COD >=25 AND BEN_AMA_COD<=29) then "[25-29]"
		when (BEN_AMA_COD >= 30 AND BEN_AMA_COD<=39) then "[30-39]"
		when (BEN_AMA_COD >= 40 AND BEN_AMA_COD<=49)  then "[40-49]"
		when (BEN_AMA_COD >= 50 AND BEN_AMA_COD<=59)  then "[50-59]"
		when (BEN_AMA_COD >= 60 AND BEN_AMA_COD<=69)  then "[60-69]"
		when (BEN_AMA_COD >= 70 AND BEN_AMA_COD<=79)  then "[70-79]"
		when (BEN_AMA_COD >= 80 AND BEN_AMA_COD<=89)  then "[80-89]"
		when (BEN_AMA_COD >= 90 AND BEN_AMA_COD< 150)  then "[90+]"
		else "inconnu" 	end as classe_age,
		count (distinct (ben_nir_psa||put(ben_rng_gem,2.))) as nb_benef,
		sum(MAX(qtt_forf_med,qtt_forf_pharm)) as qtt_IVG
from sasdata1.IVG
GROUP BY  1,2;
quit;

/* faire touner le programme de correction de michel Seguin ;*/

proc sql;
create table sasdata1.IVG_ville as select  a.*,code_insee, libelle_de_la_commune 
from sasdata1.ivg2 a, prfext.t_fin_geo_loc_france 
where codcom=code_jointure;
run;


/******* extraction des IVG hospitali�re ;



/*Programme IVG partie II*/
/*date : 12/12/2016*/
/*Base de don�nes PMSI*/

/*maj : 09/02/2017; 22/09/2017 : PMSI 2017 en entier */


***sejours IVG, on se rapporte � l'acte pour dissocier IVG m�dicamenteuses, des IVG instrumentales. UN m�me s�jour peut contenir les 2 TOP, on privil�gie la chir sur le m�dicamenteux � ce moment l� */   ;
PROC SQL;
drop table sej_IVG;
%connectora;
create table sej_IVG as
select * from connection to oracle (
	SELECT t1.ETA_NUM,
		t1.GRG_GHM,
		t4.soc_rai, 
		T1.BDI_COD,
		T1.age_ANN,
		case when T3.cdc_act='JNJP001' then 1 else 0 end as top_med,/* IVG medicamenteuse*/ 
		case when T3.cdc_act='JNJD002' then 1 else 0 end as top_chir, /* IVG instrumentales*/ 
		count (distinct (T1.ETA_NUM||t1.RSA_NUM)) as nb_sej, /* on compte les s�jours*/
		count(distinct T2.NIR_ANO_17) as nb_patient,
		sum(T3.NBR_EXE_ACT) as nb_acte
		FROM  T_MCO17B t1, T_MCO17C t2, T_MCO17A t3, T_MCO17E t4
	WHERE t1.rsa_num=t2.rsa_num 
	AND T1.eta_num=t2.eta_num
	and t1.eta_num=T4.eta_num
	and t1.eta_num=T3.eta_num
	and t1.rsa_num=t3.rsa_num
	AND t1.GRG_GHM in ('14Z08Z') 
	and substr(T1.bdi_cod,1,2) in ('14','50','61','27','76') /**** � changer ***/
	 and T3.cdc_act in ('JNJP001', 'JNJD002') 
	and T3.pha_act='0'
	and T3.acv_act='1'
	and T1.eta_num  not in
('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', '750041543', '750100018', '750100042', '750100075',
'750100083', '750100091', '750100109', '750100125', '750100166', '750100208', '750100216', '750100232', '750100273', '750100299' ,
'750801441', '750803447', '750803454', '910100015', '910100023', '920100013', '920100021', '920100039', '920100047', '920100054',
'920100062', '930100011', '930100037', '930100045', '940100027', '940100035', '940100043', '940100050', '940100068', '950100016',
'690783154', '690784137', '690784152', '690784178', '690787478', '830100558')
	GROUP BY t1.ETA_NUM,
		t1.GRG_GHM,
		t4.soc_rai, 
		T1.BDI_COD,
		T1.age_ANN,
		case when T3.cdc_act='JNJP001' then 1 else 0 end,
		case when T3.cdc_act='JNJD002' then 1 else 0 end 
	   );
  disconnect from oracle;
  quit;



data sej_IVG; set sej_IVG; 
length classe_age $50;
if age_ann<18 then classe_age ="[0-17]"; 
if 18 <=age_ann<=24 then classe_age='[18-24]';
if 25 <=age_ann<=29 then classe_age='[25-29]';
if 30 <=age_ann<=39 then classe_age='[30-39]';
if 40 <=age_ann<=49 then classe_age='[40-49]';
if 50 <=age_ann<=59 then classe_age='[50-59]';
if 60 <=age_ann<=69 then classe_age='[60-69]';
if 70 <=age_ann<=79 then classe_age='[70-79]';
if 80 <=age_ann<=89 then classe_age='[80-89]';
if age_ann> 90 then classe_age='[90+]';
run; 





