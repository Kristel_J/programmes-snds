/*********************************************************************************/
/************                   SELECTION DE DONNEES SOCIODEMOGRAPHIQUES   *******/
/************                    DANS LES REFERENTIELS DES BENEFICIAIRES   *******/
/*********************************************************************************/
/* DONNEES en entrée 
				&table_entree à définir
				ORAVUE.IR_BEN_R
				ORAVUE.IR_BEN_R_ARC
/* DONNEES en sortie
				&table_sortie à définir
/* PLAN du PROGRAMME ;
	  			- Définition par l'utilisateur des paramètres suivants:
					* table_entree= table de population dont on veut connaître
 les caractéristiques
					* table_sortie= table après croisement avec les référentiels
		
	  			- Liste des variables d'intérêt dans les référentiels
				- Copie de la table entrée dans ORAUSER
	  			- Croisement des données avec IR_BEN_R
				- Croisement des données avec IR_BEN_R_ARC (référentiel archivé) 
	  			- Compilation des deux tables
				- Conservation de la dernière information mise à jour pour chaque
 BEN_NIR_PSA/BEN_RNG_GEM
	 
/*********************************************************************/
/*********************************************************************/
/*****     CROISEMENT AVEC LE REFERENTIEL DES BENEFICIAIRES   ********/
/*********************************************************************/ 

%let table_entree=table_medicaments;
%let table_sortie=table_IR;

/*copie de la table dans ORAUSER*/
%KILL_ORAUSER( &table_entree) ;

PROC SQL; 
	CREATE TABLE ORAUSER.&table_entree AS
	SELECT	* /*ou variables à choisir, au minimum BEN_NIR_PSA, BEN_RNG_GEM*/
	FROM 	&table_entree
	;
QUIT ;


/*Croisement avec les référentiels
- IR_BEN_R pour les individus ayant consommé au moins 1 fois depuis le 1er janvier 2013
- IR_BEN_R_ARC pour les individus dont les derniers remboursements sont antérieurs à 2013*/

%let liste_var_IR_BEN =
	ben_nai_moi,
	ben_sex_cod,
	ben_dcd_dte, /*sur profil 108*/
	ben_nir_ano,
	ben_idt_ano,
	ben_dte_maj,
	max_trt_dtd
	;

proc sql ;

/*Croisement avec IR_BEN_R*/
create table tab_IR_BEN (compress=yes) as 
select a.*,&liste_var_IR_BEN
from   ORAUSER.&table_entree a , ORAVUE.IR_BEN_R  b 
where a.ben_nir_psa = b.ben_nir_psa and
	 a.ben_rng_gem = b.ben_rng_gem
;

/*Croisement avec IR_BEN_R_ARC*/
create table tab_IR_BEN_ARC (compress=yes) as 
select  a.*, &liste_var_IR_BEN
from   ORAUSER.&table_entree a  , ORAVUE.IR_BEN_R_ARC  b 

where a.ben_nir_psa = b.ben_nir_psa and
	  a.ben_rng_gem = b.ben_rng_gem;
quit;


/*Compilation des 2 tables*/
data tab_IR;set tab_IR_BEN (in=a) tab_IR_BEN_ARC(in=b);
if b then source='IR_BEN_ARC';if a then source='IR_BEN';run;


/*Conservation de l'information la plus récente par ben_nir_psa/ben_rng_gem*/
proc sort data=tab_IR;by ben_nir_psa ben_rng_gem descending ben_dte_maj descending max_trt_dtd;run;

proc sort data=tab_IR nodupkey out=&table_sortie; by ben_nir_psa ben_rng_gem;run;
