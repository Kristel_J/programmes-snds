/* ---------------------------------------------------------------------------------------------------------------------------
---------------------------------- Maternit� Nord Mayenne - Partie soins de ville - 2017 -----------------------------------
--------------------------------------------------------------------------------------------------------------------------- */


/* ----------------------------- PARTIE 1 : ACTES DES GYNECOLOGUES ET PEDIATRES DU TO 53_02 ----------------------------- */

/* Cr�ation de la table de tous les actes des gyn�cologues et p�diatres du 53 */
PROC SQL;
   DROP TABLE ORAUSER.ACTES_53_02_PED_GYN_2017;
   CREATE TABLE ORAUSER.ACTES_53_02_PED_GYN_2017 AS 
   SELECT t1.BEN_AMA_COD, t1.BEN_IDT_ANO, t1.BEN_IDT_TOP, t1.BEN_RES_COM, t1.BEN_RES_DPT, t1.CLE_DCI_JNT, t1.EXE_SOI_AMD, 
          t1.EXE_SOI_AMF, t1.EXE_SOI_DTD, t1.EXE_SOI_DTF, t1.PFS_EXE_NUM, t1.ETB_EXE_FIN, t1.PRS_NAT_REF, t1.PRS_PAI_MNT, 
          t1.PSE_ACT_NAT, t1.PSE_ACT_SPE, t1.PSE_CNV_COD, t1.PSE_SPE_COD, t1.PSE_STJ_COD, t1.PRS_ACT_NBR, t1.PRS_ACT_QTE, 
          t1.TOT_REM_BSE, t1.TOT_REM_MNT,
          (SUBSTR(t1.BEN_RES_DPT,2,2) || t1.BEN_RES_COM) AS Codecom, 
            (CASE WHEN t1.BEN_RES_DPT='053' THEN t1.BEN_RES_COM ELSE '' END) AS Codecom3_53
   FROM ORAVUE.NS_PRS_F t1
   WHERE t1.PSE_ACT_SPE IN (7,70,79,12)
          AND t1.EXE_INS_DPT = '053'
          AND t1.EXE_SOI_DTD BETWEEN '1Jan2017:0:0:0'dt AND '31Dec2017:0:0:0'dt
          AND t1.FLX_TRT_DTD BETWEEN '1Jan2017:0:0:0'dt AND '30Jun2018:0:0:0'dt;
QUIT;

/* Cr�ation de la table de tous les actes des gyn�cologues et p�diatres du 53, avec notion du 53_02 et du dpt du patient */
PROC SQL;
   DROP TABLE ORAUSER.ACTES2_53_02_PED_GYN_2017;
   CREATE TABLE ORAUSER.ACTES2_53_02_PED_GYN_2017 AS 
   SELECT t1.BEN_AMA_COD, t1.BEN_IDT_ANO, t1.BEN_IDT_TOP, t1.BEN_RES_COM, t1.BEN_RES_DPT, t1.CLE_DCI_JNT, t1.EXE_SOI_AMD, 
          t1.EXE_SOI_AMF, t1.EXE_SOI_DTD, t1.EXE_SOI_DTF, t1.PFS_EXE_NUM, t1.ETB_EXE_FIN, t1.PRS_NAT_REF, t1.PRS_PAI_MNT, 
          t1.PSE_ACT_NAT, t1.PSE_ACT_SPE, t1.PSE_CNV_COD, t1.PSE_SPE_COD, t1.PSE_STJ_COD, t1.PRS_ACT_NBR, t1.PRS_ACT_QTE, 
          t1.TOT_REM_BSE, t1.TOT_REM_MNT, Codecom, Codecom3_53,
		  /* 53_02 */
            (CASE WHEN t1.Codecom3_53 IN ('002','003','005','008','013','015','021','028','031','038','042','047','048','051',
            '052','055','057','061','064','069','071','072','074','079','080','083','085','086','091','093','096','100','106',
            '107','109','111','112','114','115','116','118','121','122','123','125','126','127','131','132','133','139','142',
            '144','146','147','154','155','160','162','164','170','173','174','176','177','179','181','185','187','189','190',
            '195','196','198','199','200','202','204','208','211','213','216','219','222','223','225','226','230','234','235',
            '236','237','238','245','246','261','263','269','270','271','272') THEN '53_02'
               ELSE 'Hors_53_02' END) AS TO_53_02, 
          /* Dep_53_ou_autre */
            (CASE WHEN t1.BEN_RES_DPT IN ('044','049','072','085') THEN 'Autre_dpt_que_53' ELSE 'Dpt_53' END) AS Dpt_53, 
          /* concat */
            (CASE WHEN t1.BEN_RES_DPT IN ('044','049','072','085') THEN 'Autre_dpt_que_53' ELSE 'Dpt_53' END) ||
            (CASE WHEN t1.Codecom3_53 IN 
            (
            '002','003','005','008','013','015','021','028','031','038','042','047','048','051',
            '052','055','057','061','064','069','071','072','074','079','080','083','085','086','091','093','096','100','106',
            '107','109','111','112','114','115','116','118','121','122','123','125','126','127','131','132','133','139','142',
            '144','146','147','154','155','160','162','164','170','173','174','176','177','179','181','185','187','189','190',
            '195','196','198','199','200','202','204','208','211','213','216','219','222','223','225','226','230','234','235',
            '236','237','238','245','246','261','263','269','270','271','272'
            ) THEN '53_02'
               ELSE 'Hors_53_02'
            END) AS concat
   FROM ORAUSER.ACTES_53_02_PED_GYN_2017 t1;

/* Table des gyn�cologues et p�diatres ayant exerc� en 2017 pour r�cup�rer ensuite leur code commune d'exercice */
PROC SQL;
   DROP TABLE ORAUSER.PED_GYN_53_02_2017;
   CREATE TABLE ORAUSER.PED_GYN_53_02_2017 AS  
   SELECT DISTINCT t1.PFS_PFS_NUM, t1.PFS_PRA_SPE, t1.CAI_NUM, t1.PFS_EXC_COM,
            (SUBSTR(t1.CAI_NUM,1,2) || t1.PFS_EXC_COM) AS Codecom, 
            (CASE WHEN t1.CAI_NUM='531' THEN t1.PFS_EXC_COM ELSE '' END) AS Codecom3_53,
			(CASE WHEN t1.PFS_PRA_SPE IN ('12') THEN 'PEDIATRE'
                  WHEN t1.PFS_PRA_SPE IN ('07','70','79') THEN 'GYNECOLOGUE'
				  WHEN t1.PFS_PRA_SPE IN ('21') THEN 'SAGE-FEMME'
                  WHEN t1.PFS_PRA_SPE IN ('01','22','23') THEN 'GENERALISTE'
                  ELSE '' END) AS Specialite
      FROM ORAVUE.DA_PRA_R t1
      WHERE t1.DTE_ANN_TRT = '2017' AND t1.CAI_NUM = '531' AND t1.PFS_PRA_SPE IN ('12','07','70','79')
           AND t1.PFS_EXC_COM IN 
           (
           '002','003','005','008','013','015','021','028','031','038','042','047','048','051',
            '052','055','057','061','064','069','071','072','074','079','080','083','085','086','091','093','096','100','106',
            '107','109','111','112','114','115','116','118','121','122','123','125','126','127','131','132','133','139','142',
            '144','146','147','154','155','160','162','164','170','173','174','176','177','179','181','185','187','189','190',
            '195','196','198','199','200','202','204','208','211','213','216','219','222','223','225','226','230','234','235',
            '236','237','238','245','246','261','263','269','270','271','272'
           );
QUIT;

/* Cr�ation de la table de tous les actes de gyn�cologues et p�diatres du 53_02 */
PROC SQL;
   DROP TABLE ORAUSER.ACTES3_53_02_PED_GYN_2017;
   CREATE TABLE ORAUSER.ACTES3_53_02_PED_GYN_2017 AS 
   SELECT t1.BEN_AMA_COD, t1.BEN_IDT_ANO, t1.BEN_IDT_TOP, t1.BEN_RES_COM, t1.BEN_RES_DPT, t1.CLE_DCI_JNT, t1.EXE_SOI_AMD, 
          t1.EXE_SOI_AMF, t1.EXE_SOI_DTD, t1.EXE_SOI_DTF, t1.PFS_EXE_NUM, t1.ETB_EXE_FIN, t1.PRS_NAT_REF, t1.PRS_PAI_MNT, 
          t1.PSE_ACT_NAT, t1.PSE_ACT_SPE, t1.PSE_CNV_COD, t1.PSE_SPE_COD, t1.PSE_STJ_COD, t1.PRS_ACT_NBR, t1.PRS_ACT_QTE, 
          t1.TOT_REM_BSE, t1.TOT_REM_MNT, t1.Codecom, t1.Codecom3_53, t1.TO_53_02, t1.Dpt_53, t1.concat,
		  t2.PFS_PFS_NUM, t2.PFS_PRA_SPE, t2.CAI_NUM, t2.PFS_EXC_COM, t2.Codecom, t2.Codecom3_53, t2.Specialite
      FROM ORAUSER.ACTES2_53_02_PED_GYN_2017 t1, ORAUSER.PED_GYN_53_02_2017 t2
	  WHERE t1.PFS_EXE_NUM=t2.PFS_PFS_NUM;
QUIT;

/* Calcul des indicateurs*/
/* 1. D�tail par PS*/
PROC SQL;
   DROP TABLE ORAUSER.TAB_GYN_PED_53_02_2017;
   CREATE TABLE ORAUSER.TAB_GYN_PED_53_02_2017 AS 
   SELECT t1.Specialite,
          (COUNT(DISTINCT(t1.BEN_IDT_ANO))) AS NB_PATIENTS, 
          (SUM(t1.PRS_ACT_QTE)) AS NB_ACTES, 
          (SUM(t1.TOT_REM_BSE)) AS BASE_REMB
      FROM ORAUSER.ACTES3_53_02_PED_GYN_2017 t1
      GROUP BY t1.Specialite;
QUIT;
/* 2. D�tail par commune de r�sidence du patient*/
PROC SQL;
   DROP TABLE ORAUSER.TAB2_GYN_PED_53_02_2017;
   CREATE TABLE ORAUSER.TAB2_GYN_PED_53_02_2017 AS 
   SELECT t1.concat,
          (COUNT(DISTINCT(t1.BEN_IDT_ANO))) AS NB_PATIENTS, 
          (SUM(t1.PRS_ACT_QTE)) AS NB_ACTES, 
          (SUM(t1.TOT_REM_BSE)) AS BASE_REMB
      FROM ORAUSER.ACTES3_53_02_PED_GYN_2017 t1
      GROUP BY t1.concat;
QUIT;


/* ----------------------- PARTIE 2 : CONSOMMATION DE SOINS DES ENFANTS ET DES FEMMES DU TO 53_02 ----------------------- */

/* Consommation de soins gyn�co et de SF des femmes de 15 � 49 ans du 53_02 */
PROC SQL;
   DROP TABLE ORAUSER.CONSO_FEMMES_53_02_2017;
   CREATE TABLE ORAUSER.CONSO_FEMMES_53_02_2017 AS 
   SELECT t1.BEN_IDT_ANO, t1.BEN_SEX_COD, t1.BEN_NAI_ANN, t1.BEN_RES_DPT, t1.BEN_RES_COM,
          t2.BEN_AMA_COD, t2.BEN_IDT_ANO, t2.BEN_IDT_TOP, t2.BEN_RES_COM, t2.BEN_RES_DPT, t2.CLE_DCI_JNT,
          t2.EXE_SOI_AMD, t2.EXE_SOI_AMF, t2.EXE_SOI_DTD, t2.EXE_SOI_DTF, t2.PFS_EXE_NUM, t2.ETB_EXE_FIN,
          t2.PRS_NAT_REF, t2.PRS_PAI_MNT, t2.PSE_ACT_NAT, t2.PSE_ACT_SPE, t2.PSE_CNV_COD, t2.PSE_SPE_COD,
          t2.PSE_STJ_COD, t2.PRS_ACT_NBR, t2.PRS_ACT_QTE, t2.TOT_REM_BSE, t2.TOT_REM_MNT
      FROM ORAVUE.IR_IBA_R t1, ORAVUE.NS_PRS_F t2
      WHERE t1.BEN_IDT_ANO=t2.BEN_IDT_ANO
          AND t1.BEN_SEX_COD = 2 AND t1.BEN_NAI_ANN <= '2002' AND t1.BEN_NAI_ANN >= '1968'
          AND t1.BEN_RES_DPT = '053' AND 
           t1.BEN_RES_COM IN 
           (
           '002','003','005','008','013','015','021','028','031','038','042','047','048','051',
            '052','055','057','061','064','069','071','072','074','079','080','083','085','086','091','093','096','100','106',
            '107','109','111','112','114','115','116','118','121','122','123','125','126','127','131','132','133','139','142',
            '144','146','147','154','155','160','162','164','170','173','174','176','177','179','181','185','187','189','190',
            '195','196','198','199','200','202','204','208','211','213','216','219','222','223','225','226','230','234','235',
            '236','237','238','245','246','261','263','269','270','271','272'
           )
          AND t2.BEN_RES_DPT = '053'
          AND t2.PSE_ACT_SPE IN (21,7,70,79)
          AND t2.EXE_SOI_DTD BETWEEN '1Jan2017:0:0:0'dt AND '31Dec2017:0:0:0'dt
          AND t2.FLX_TRT_DTD BETWEEN '1Jan2017:0:0:0'dt AND '30Jun2018:0:0:0'dt;
QUIT;

/* Consommation de soins de p�diatres et de g�n�ralistes des enfants de 0 � 6 ans du 53_02 */
PROC SQL;
   DROP TABLE ORAUSER.CONSO_ENFANTS_53_02_2017;
   CREATE TABLE ORAUSER.CONSO_ENFANTS_53_02_2017 AS 
   SELECT t1.BEN_IDT_ANO, t1.BEN_SEX_COD, t1.BEN_NAI_ANN, t1.BEN_RES_DPT, t1.BEN_RES_COM,
          t2.BEN_AMA_COD, t2.BEN_IDT_ANO, t2.BEN_IDT_TOP, t2.BEN_RES_COM, t2.BEN_RES_DPT, t2.CLE_DCI_JNT,
          t2.EXE_SOI_AMD, t2.EXE_SOI_AMF, t2.EXE_SOI_DTD, t2.EXE_SOI_DTF, t2.PFS_EXE_NUM, t2.ETB_EXE_FIN,
          t2.PRS_NAT_REF, t2.PRS_PAI_MNT, t2.PSE_ACT_NAT, t2.PSE_ACT_SPE, t2.PSE_CNV_COD, t2.PSE_SPE_COD,
          t2.PSE_STJ_COD, t2.PRS_ACT_NBR, t2.PRS_ACT_QTE, t2.TOT_REM_BSE, t2.TOT_REM_MNT
      FROM ORAVUE.IR_IBA_R t1, ORAVUE.NS_PRS_F t2
      WHERE t1.BEN_IDT_ANO=t2.BEN_IDT_ANO
          AND t1.BEN_NAI_ANN >= '2011'
          AND t1.BEN_RES_DPT = '053' AND 
           t1.BEN_RES_COM IN 
           (
           '002','003','005','008','013','015','021','028','031','038','042','047','048','051',
            '052','055','057','061','064','069','071','072','074','079','080','083','085','086','091','093','096','100','106',
            '107','109','111','112','114','115','116','118','121','122','123','125','126','127','131','132','133','139','142',
            '144','146','147','154','155','160','162','164','170','173','174','176','177','179','181','185','187','189','190',
            '195','196','198','199','200','202','204','208','211','213','216','219','222','223','225','226','230','234','235',
            '236','237','238','245','246','261','263','269','270','271','272'
           )
          AND t2.BEN_RES_DPT = '053'
          AND t2.PSE_ACT_SPE IN (12,1,22,23)
          AND t2.EXE_SOI_DTD BETWEEN '1Jan2017:0:0:0'dt AND '31Dec2017:0:0:0'dt
          AND t2.FLX_TRT_DTD BETWEEN '1Jan2017:0:0:0'dt AND '30Jun2018:0:0:0'dt;
QUIT;

/* Table des PS ayant exerc� en 2017 pour r�cup�rer ensuite leur code commune d'exercice */
PROC SQL;
   DROP TABLE ORAUSER.PS_53_02_2017;
   CREATE TABLE ORAUSER.PS_53_02_2017 AS 
   SELECT DISTINCT t1.PFS_PFS_NUM, t1.PFS_PRA_SPE, t1.CAI_NUM, t1.PFS_EXC_COM, 
            (SUBSTR(t1.CAI_NUM,1,2) || t1.PFS_EXC_COM) AS Codecom, 
            (CASE WHEN t1.CAI_NUM='531' THEN t1.PFS_EXC_COM ELSE '' END) AS Codecom3_53,
			(CASE WHEN t1.PFS_PRA_SPE IN ('12') THEN 'PEDIATRE'
                  WHEN t1.PFS_PRA_SPE IN ('07','70','79') THEN 'GYNECOLOGUE'
				  WHEN t1.PFS_PRA_SPE IN ('21') THEN 'SAGE-FEMME'
                  WHEN t1.PFS_PRA_SPE IN ('01','22','23') THEN 'GENERALISTE'
                  ELSE '' END) AS Specialite
	  FROM ORAVUE.DA_PRA_R t1
      WHERE t1.DTE_ANN_TRT = '2017'
          AND t1.CAI_NUM IN ('441','491','531','721','851')
          AND t1.PFS_PRA_SPE IN ('12','07','70','79','21','01','22','23')
      ORDER BY t1.PFS_PFS_NUM;
QUIT;

/* Table des PS ayant exerc� en 2017 pour r�cup�rer ensuite leur code commune d'exercice, avec notion du 53_02 et du dpt du PS */
PROC SQL;
   DROP TABLE ORAUSER.PS2_53_02_2017;
   CREATE TABLE ORAUSER.PS2_53_02_2017 AS 
   SELECT DISTINCT t1.PFS_PFS_NUM, t1.PFS_PRA_SPE, t1.CAI_NUM, t1.PFS_EXC_COM, t1.Codecom, t1.Codecom3_53, t1.Specialite,
		  /* 53_02 */
            (CASE WHEN t1.Codecom3_53 IN ('002','003','005','008','013','015','021','028','031','038','042','047','048','051',
            '052','055','057','061','064','069','071','072','074','079','080','083','085','086','091','093','096','100','106',
            '107','109','111','112','114','115','116','118','121','122','123','125','126','127','131','132','133','139','142',
            '144','146','147','154','155','160','162','164','170','173','174','176','177','179','181','185','187','189','190',
            '195','196','198','199','200','202','204','208','211','213','216','219','222','223','225','226','230','234','235',
            '236','237','238','245','246','261','263','269','270','271','272') THEN '53_02'
               ELSE 'Hors_53_02' END) AS TO_53_02, 
          /* Dep_53_ou_autre */
            (CASE WHEN t1.CAI_NUM IN ('441','491','721','851') THEN 'Autre_dpt_que_53' ELSE 'Dpt_53' END) AS Dpt_53, 
          /* concat */
            (CASE WHEN t1.CAI_NUM IN ('441','491','721','851') THEN 'Autre_dpt_que_53' ELSE 'Dpt_53' END) ||
            (CASE WHEN t1.Codecom3_53 IN 
            (
            '002','003','005','008','013','015','021','028','031','038','042','047','048','051',
            '052','055','057','061','064','069','071','072','074','079','080','083','085','086','091','093','096','100','106',
            '107','109','111','112','114','115','116','118','121','122','123','125','126','127','131','132','133','139','142',
            '144','146','147','154','155','160','162','164','170','173','174','176','177','179','181','185','187','189','190',
            '195','196','198','199','200','202','204','208','211','213','216','219','222','223','225','226','230','234','235',
            '236','237','238','245','246','261','263','269','270','271','272'
            ) THEN '53_02'
               ELSE 'Hors_53_02'
            END) AS concat
      FROM ORAUSER.PS_53_02_2017 t1;
QUIT;

/* On r�cup�re le code commune d'exercice des PS dans la table de conso des femmes*/
PROC SQL;
   DROP TABLE ORAUSER.CONSO2_FEMMES_53_02_2017;
   CREATE TABLE ORAUSER.CONSO2_FEMMES_53_02_2017 AS 
   SELECT DISTINCT t1.PFS_PFS_NUM, t1.PFS_PRA_SPE, t1.CAI_NUM, t1.PFS_EXC_COM, t1.Codecom, t1.Codecom3_53, t1.Specialite, t1.concat,
		  t2.BEN_IDT_ANO, t2.BEN_SEX_COD, t2.BEN_NAI_ANN, t2.BEN_RES_DPT, t2.BEN_RES_COM,
          t2.BEN_AMA_COD, t2.BEN_IDT_ANO, t2.BEN_IDT_TOP, t2.BEN_RES_COM, t2.BEN_RES_DPT, t2.CLE_DCI_JNT,
          t2.EXE_SOI_AMD, t2.EXE_SOI_AMF, t2.EXE_SOI_DTD, t2.EXE_SOI_DTF, t2.PFS_EXE_NUM, t2.ETB_EXE_FIN,
          t2.PRS_NAT_REF, t2.PRS_PAI_MNT, t2.PSE_ACT_NAT, t2.PSE_ACT_SPE, t2.PSE_CNV_COD, t2.PSE_SPE_COD,
          t2.PSE_STJ_COD, t2.PRS_ACT_NBR, t2.PRS_ACT_QTE, t2.TOT_REM_BSE, t2.TOT_REM_MNT
      FROM ORAUSER.PS2_53_02_2017 t1, ORAUSER.CONSO_FEMMES_53_02_2017 t2
      WHERE t1.PFS_PFS_NUM=t2.PFS_EXE_NUM;
QUIT;

/* On r�cup�re le code commune d'exercice des PS dans la table de conso des enfants*/
PROC SQL;
   DROP TABLE ORAUSER.CONSO2_ENFANTS_53_02_2017;
   CREATE TABLE ORAUSER.CONSO2_ENFANTS_53_02_2017 AS 
   SELECT DISTINCT t1.PFS_PFS_NUM, t1.PFS_PRA_SPE, t1.CAI_NUM, t1.PFS_EXC_COM, t1.Codecom, t1.Codecom3_53, t1.Specialite, t1.concat,
		  t2.BEN_IDT_ANO, t2.BEN_SEX_COD, t2.BEN_NAI_ANN, t2.BEN_RES_DPT, t2.BEN_RES_COM,
          t2.BEN_AMA_COD, t2.BEN_IDT_ANO, t2.BEN_IDT_TOP, t2.BEN_RES_COM, t2.BEN_RES_DPT, t2.CLE_DCI_JNT,
          t2.EXE_SOI_AMD, t2.EXE_SOI_AMF, t2.EXE_SOI_DTD, t2.EXE_SOI_DTF, t2.PFS_EXE_NUM, t2.ETB_EXE_FIN,
          t2.PRS_NAT_REF, t2.PRS_PAI_MNT, t2.PSE_ACT_NAT, t2.PSE_ACT_SPE, t2.PSE_CNV_COD, t2.PSE_SPE_COD,
          t2.PSE_STJ_COD, t2.PRS_ACT_NBR, t2.PRS_ACT_QTE, t2.TOT_REM_BSE, t2.TOT_REM_MNT
      FROM ORAUSER.PS2_53_02_2017 t1, ORAUSER.CONSO_ENFANTS_53_02_2017 t2
      WHERE t1.PFS_PFS_NUM=t2.PFS_EXE_NUM;
QUIT;

/* Calcul d'indicateurs pour les femmes*/
/* 0. Nombre de femmes*/
PROC SQL;
   DROP TABLE ORAUSER.TAB_FEMMES_0_53_02_2017;
   CREATE TABLE ORAUSER.TAB_FEMMES_0_53_02_2017 AS 
   SELECT (COUNT(DISTINCT(t1.BEN_IDT_ANO))) AS NB_FEMMES
      FROM ORAVUE.IR_IBA_R t1
      WHERE t1.BEN_SEX_COD = 2 AND t1.BEN_NAI_ANN <= '2002' AND t1.BEN_NAI_ANN >= '1968'
            AND t1.BEN_RES_DPT = '053' AND t1.BEN_RES_COM IN 
           (
           '002','003','005','008','013','015','021','028','031','038','042','047','048','051',
            '052','055','057','061','064','069','071','072','074','079','080','083','085','086','091','093','096','100','106',
            '107','109','111','112','114','115','116','118','121','122','123','125','126','127','131','132','133','139','142',
            '144','146','147','154','155','160','162','164','170','173','174','176','177','179','181','185','187','189','190',
            '195','196','198','199','200','202','204','208','211','213','216','219','222','223','225','226','230','234','235',
            '236','237','238','245','246','261','263','269','270','271','272'
           );
QUIT;
/* 1. D�tail par PS*/
PROC SQL;
   DROP TABLE ORAUSER.TAB_FEMMES_53_02_2017;
   CREATE TABLE ORAUSER.TAB_FEMMES_53_02_2017 AS 
   SELECT t1.Specialite,
          (COUNT(DISTINCT(t1.BEN_IDT_ANO))) AS NB_FEMMES, 
          (SUM(t1.PRS_ACT_QTE)) AS NB_ACTES, 
          (SUM(t1.TOT_REM_BSE)) AS BASE_REMB
      FROM ORAUSER.CONSO2_FEMMES_53_02_2017 t1
      GROUP BY t1.Specialite;
QUIT;
/* 2. D�tail par commune d'exercice du PS*/
PROC SQL;
   DROP TABLE ORAUSER.TAB2_FEMMES_53_02_2017;
   CREATE TABLE ORAUSER.TAB2_FEMMES_53_02_2017 AS 
   SELECT t1.concat,
          (COUNT(DISTINCT(t1.BEN_IDT_ANO))) AS NB_FEMMES, 
          (SUM(t1.PRS_ACT_QTE)) AS NB_ACTES, 
          (SUM(t1.TOT_REM_BSE)) AS BASE_REMB
      FROM ORAUSER.CONSO2_FEMMES_53_02_2017 t1
      GROUP BY t1.concat;
QUIT;

/* Calcul d'indicateurs pour les enfants*/
/* 0. Nombre d'enfants*/
PROC SQL;
   DROP TABLE ORAUSER.TAB_ENFANTS_0_53_02_2017;
   CREATE TABLE ORAUSER.TAB_ENFANTS_0_53_02_2017 AS 
   SELECT (COUNT(DISTINCT(t1.BEN_IDT_ANO))) AS NB_ENFANTS
      FROM ORAVUE.IR_IBA_R t1
      WHERE t1.BEN_NAI_ANN >= '2011'
            AND t1.BEN_RES_DPT = '053' AND t1.BEN_RES_COM IN 
           (
           '002','003','005','008','013','015','021','028','031','038','042','047','048','051',
            '052','055','057','061','064','069','071','072','074','079','080','083','085','086','091','093','096','100','106',
            '107','109','111','112','114','115','116','118','121','122','123','125','126','127','131','132','133','139','142',
            '144','146','147','154','155','160','162','164','170','173','174','176','177','179','181','185','187','189','190',
            '195','196','198','199','200','202','204','208','211','213','216','219','222','223','225','226','230','234','235',
            '236','237','238','245','246','261','263','269','270','271','272'
           );
QUIT;
/* 1. D�tail par PS*/
PROC SQL;
   DROP TABLE ORAUSER.TAB_ENFANTS_53_02_2017;
   CREATE TABLE ORAUSER.TAB_ENFANTS_53_02_2017 AS 
   SELECT t1.Specialite,
          (COUNT(DISTINCT(t1.BEN_IDT_ANO))) AS NB_ENFANTS, 
          (SUM(t1.PRS_ACT_QTE)) AS NB_ACTES, 
          (SUM(t1.TOT_REM_BSE)) AS BASE_REMB
      FROM ORAUSER.CONSO2_ENFANTS_53_02_2017 t1
      GROUP BY t1.Specialite;
QUIT;
/* 2. D�tail par commune d'exercice du PS*/
PROC SQL;
   DROP TABLE ORAUSER.TAB2_ENFANTS_53_02_2017;
   CREATE TABLE ORAUSER.TAB2_ENFANTS_53_02_2017 AS 
   SELECT t1.concat,
          (COUNT(DISTINCT(t1.BEN_IDT_ANO))) AS NB_ENFANTS, 
          (SUM(t1.PRS_ACT_QTE)) AS NB_ACTES, 
          (SUM(t1.TOT_REM_BSE)) AS BASE_REMB
      FROM ORAUSER.CONSO2_ENFANTS_53_02_2017 t1
      GROUP BY t1.concat;
QUIT;