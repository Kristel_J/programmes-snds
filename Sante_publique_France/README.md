# Santé publique France

Santé publique France est l’agence nationale de santé publique. 
Créée en mai 2016 par ordonnance et décret, c’est un établissement public administratif sous tutelle du ministère chargé de la Santé. 
Sa mission : améliorer et protéger la santé des populations.

## Mainteneurs du dossier:

- Laurence Mandereau-Bruno: [laurence.mandereau-bruno@santepubliquefrance.fr](laurence.mandereau-bruno@santepubliquefrance.fr)
- Elodie Moutengou: <elodie.moutengou@santepubliquefrance.fr>

## Liste des programmes mis à disposition

### Programme "prestations_medicaments_dcir.sas"

Ce programme permet la sélection de prestations affinées en prenant comme exemple la table des médicaments.  
L'utilisateur choisit la liste des médicaments à sélectionner et la période d'intérêt.  
Il peut être adapté pour une autre table affinée (exemple : biologie, dispositifs médicaux...)

Détails et explications dans la fiche de documentation
[Requête type dans la table prestations du DCIR](https://documentation-snds.health-data-hub.fr/fiches/sas_prestation_dcir.html).


### Programme "infos_socio_eco_referentiels_dcir.sas"

Ce programme permet d'aller rechercher les informations socio-démographiques présentes dans les référentiels des bénéficiaires pour les individus d'une table déjà créée.  


Détails et explications dans la fiche de documentation
[Requête type dans la table prestations du DCIR](https://documentation-snds.health-data-hub.fr/fiches/sas_prestation_dcir.html).

### Programme "sejours_pmsi_mco.sas"

Ce programme permet la sélection de séjours à partir de codes diagnostics (principal ou relié ou associés).  
Pour ces séjours, un certain nombre de variables décrivant le séjour sélectionné sont restituées en sortie. 


Détails et explications dans la fiche de documentation
Requête type dans le PMSI-MCO (lien à venir).