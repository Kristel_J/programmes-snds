# Programmes SNDS
<!-- SPDX-License-Identifier: MPL-2.0 -->

Ce dépôt est un lieu central pour **partager le code source de programmes** de travaux et d'études réalisés sur le SNDS.

D'autres dépôts de programmes sont listés dans une [page dédiée de la documentation](https://documentation-snds.health-data-hub.fr/ressources/programmes.html).
Ce dépôt n’a ainsi pas vocation à être exclusif mais facilite le rassemblement des ressources, notamment pour les organisations qui ne disposeraient pas à ce jour de leur propre dépôt.

Ce dépôt est maintenu par le Health Data Hub <opensource@health-data-hub.fr>, qui pourra porter assistance à chaque organisation qui le souhaite dans le partage de ses programmes.

## Objectifs

L'objectif de ce dépôt est de faciliter le partage de programmes.

Le partage de programmes a de nombreux bénéfices :
- Il améliore la *prise en main des données* en permettant de s'appuyer sur les requêtes établies par d’autres, et permet d'*aborder plus rapidement de nouvelles thématiques*.
- Il facilite la *compréhension des méthodologies* employées dans des travaux publiés et améliore leur *reproductibilité*.
- Il engendre des discussions factuelles qui permettent de progresser collectivement.

Enfin, le partage met en valeur le travail de l'auteur et de son organisation, et permet à d'autres de créditer ce travail

**Note** : ce dépôt n'a pas pour objectif de faire émerger des programmes de référence.
Cette tâche serait ardue, si ce n'est impossible, car chaque programme dépend fortement de son contexte d'écriture : enjeu du travail ou de l'étude, environnement technique, année, etc.
Le maintien ou l’actualisation d'un programme est sous la responsabilité de son auteur.

## Un dossier par organisation

Chaque dossier à la racine appartient à une organisation.

Cette structure de dossiers simplifie la gouvernance et la présentation du contexte des programmes.

Pour les organisations souhaitant une plus grande autonomie, la création de dépôts dédiés est encouragée.
Ces dépôts peuvent être créés sur le [groupe healthdatahub](https://gitlab.com/healthdatahub), ou en [créant votre propre groupe d'organisation](https://gitlab.com/groups/new) pour une totale autonomie.
Le Health Data Hub <opensource@health-data-hub.fr> peut vous aider à créer ces dépôts et organiser des formations si la demande le justifie.

## Déposer ses programmes

Pour déposer des programmes, référez vous au document [CONTRIBUTING.MD](CONTRIBUTING.md) qui définie les bonnes pratiques de publication sur ce répertoire.

## AVERTISSEMENTS

- Les programmes partagés dans ce répertoire ne sont pas des publications officielles.
- Ils sont fournis sans aucune garantie explicite ou implicite.
- - Ils n'engagent aucunement les auteurs ou l'organisation à laquelle ils sont rattachés.
- Si vous utilisez ces programmes, vous assumez tous les risques quant à leur qualité et à leurs effets.

Voir également le point **7. Disclaimer of warranty** de la [LICENCE](LICENCE).

## Licence

Tous les contenu partagés dans ce dépôt sont publiés sous [licence Apache](https://fr.wikipedia.org/wiki/Licence_Apache) version 2.0.
Une copie de la licence est disponible dans le fichier [LICENCE](LICENCE), ou à l'adresse http://www.apache.org/licenses/LICENSE-2.0.  

### Explications

La licence `Apache-2.0` est une [licence libre](https://fr.wikipedia.org/wiki/Licence_libre).
Cela signifie qu'elle donne à quiconque la liberté d'utiliser, d'étudier, de modifier et de redistribuer les contenus partagés dans ce dépôt, _à condition_ que la licence et les auteurs de ces contenus soient mentionnés.

### Remarque

Lorsqu'un contenu produit dans le cadre d'une mission de service public est partagé _sans licence_, les conditions de sa réutilisation sont fixées par le [Code des Relations entre le Public et l'Administration](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000031366350).

Un guide juridique pour la publication de logiciels libres dans l'administration est disponible sur le site internet de la mission Etalab : [guide-juridique-logiciel-libre.etalab.gouv.fr/](https://guide-juridique-logiciel-libre.etalab.gouv.fr/).
